def parse_ann(file):
    persons = dict()
    quotes = dict()
    relations = dict()

    with open('./dataset/' + file, 'r') as f:
        for line in f:
            vals = line.split('\t')
            id = vals[0]
            tag, start, end = vals[1].split()
            if tag == 'Person':
                persons[id] = {'start': start, 'end': end, 'value': vals[2]}
            elif tag == 'Quote':
                quotes[id] = {'start': start, 'end': end, 'value': vals[2]}
            elif tag == 'RelatedQuote':
                relations[id] = {'person': start, 'quote': end}

    return persons, quotes, relations


def get_relations(persons, quotes, relations):
    for relation in relations.values():
        person = relation['person'].split(':')[1]
        quote = relation['quote'].split(':')[1]
        relation['person_coords'] = [int(persons[person]['start']), int(persons[person]['end'])]
        relation['person'] = persons[person]['value']
        relation['quote'] = quotes[quote]['value']
        relation['quote_coords'] = [int(quotes[quote]['start']), int(quotes[quote]['end'])]
    return relations

def contains_relation(relation, standard):
    if len(relation) == 0:
        return False
    for r in standard:
        if r['person_coords'] == relation['person_coords'] and r['quote_coords'] == relation['quote_coords']:
            return True
    return False


if __name__ == '__main__':
    p, q, r = parse_ann('228.ann')
    relations = get_relations(p, q, r)
    print(relations)
