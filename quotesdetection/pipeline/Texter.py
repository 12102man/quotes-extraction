import datetime

import pandas as pd
import nltk
import spacy
from spacy.tokens import Span
from spacy.tokens.doc import Doc
from spacy.tokens.token import Token


class Texter:
    def __init__(self, text):

        text = text.replace('\n', ' ').replace("\r", '')
        nltk.download('punkt')
        nlp = spacy.load("en")
        Doc.set_extension("quotes", default=[], force=True)
        Token.set_extension("is_quote_corner", default=False, force=True)

        nlp.add_pipe(self.quotes_detector, first=True)
        nlp.add_pipe(self.set_custom_boundaries, before='parser')
        self.doc = nlp(text)
        self.text = text
        self.filtered_text = list(
            filter(lambda x: x, text.replace('\n', ' ').replace('“', '\"').replace('”', '\"').split(' ')))
        self.data = pd.DataFrame({'tokens': self.filtered_text})
        self.out_text = None

    def persons(self):
        return list(filter(lambda x: x.label_ == 'PERSON', self.doc.ents))

    def save_persons(self, id):
        """
        Saves ners to .ann file
        :param id: id of article
        :return:
        """
        last = 0
        ROOT = './dataset/'
        with open(ROOT + f'{id}.ann', 'r') as f:
            for line in f:
                last = int(line.split('\t')[0][1:])

        with open(ROOT + f'{id}.ann', 'a') as f:
            for span in self.persons():
                last += 1
                f.write(f'T{last}\tPerson {span.start_char} {span.end_char}\t{span.text}\n')

    def save_quotes(self, id):
        """
        Saves quotes to .ann file
        :param id: id of article
        :return:
        """
        last = 0
        ROOT = './dataset/'
        with open(ROOT + f'{id}.ann', 'r') as f:
            for line in f:
                last = int(line.split('\t')[0][1:])

        with open(ROOT + f'{id}.ann', 'a') as f:
            for span in self.doc._.quotes:
                last += 1
                f.write(f'T{last}\tQuote {span.start_char} {span.end_char}\t{span.text}\n')

    @staticmethod
    def quotes_detector(doc):
        start = None
        for token in doc:
            token_text = token.text.replace(".\"", "\"").replace("\".", "\"").replace(",\"", "\"").replace("\",", "\"")
            if token_text == '\"' and not start:
                start = token
                token._.is_quote_corner = True
            elif token_text == '\"' and start:
                token._.is_quote_corner = True
                span = Span(doc, start.i + 1, token.i, label="QUOTE")
                doc._.quotes = list(doc._.quotes) + [span]
                start = None
        return doc

    def get_spacy_relations(self):
        out = []
        for sent in self.doc.sents:
            ents = list(filter(lambda x: x.label_ == 'PERSON', sent.ents))

            quotes = []
            for quote in self.doc._.quotes:
                if quote.start >= sent.start and quote.end <= sent.end:
                    quotes.append(quote)

            relations = []

            if len(ents) == 1 and len(quotes) == 1:
                relations.append({
                    'sentence': sent.text,
                    'sent_coords': [sent.start, sent.end],
                    'person': ents[0].text,
                    'person_coords': [ents[0].start_char, ents[0].end_char],
                    'quote': quotes[0].text,
                    'quote_coords': [quotes[0].start_char, quotes[0].end_char],
                })
                pass

            out.append({
                'sentence': sent.text,
                'ents': map(lambda x: x.text, ents),
                'relations': relations,
                'quotes': map(lambda x: x.text, quotes),
            })

        return out

    @staticmethod
    def set_custom_boundaries(doc):
        quote_borders = 0

        for token in doc[:-1]:
            if token._.is_quote_corner:
                quote_borders += 1
            elif token.text in ['.', '!', '?'] and quote_borders % 2 == 0:
                doc[token.i + 1].is_sent_start = True
            else:
                doc[token.i + 1].is_sent_start = False
        return doc
