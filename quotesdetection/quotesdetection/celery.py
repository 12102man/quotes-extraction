from __future__ import absolute_import, unicode_literals

from os import environ

from celery import Celery


class Config:
    broker_url: str = (
        f'amqp://{environ.get("RABBITMQ_DEFAULT_USER")}:'
        f'{environ.get("RABBITMQ_DEFAULT_PASS")}@'
        f'{environ.get("RABBITMQ_HOST")}:{environ.get("RABBITMQ_PORT")}/'
        f'{environ.get("RABBITMQ_DEFAULT_VHOST")}'
    )
    broker_api: str = (
        f'http://{environ.get("RABBITMQ_DEFAULT_USER")}:'
        f'{environ.get("RABBITMQ_DEFAULT_PASS")}@'
        f'{environ.get("RABBITMQ_HOST")}:{environ.get("RABBITMQ_PORT")}/api/'
    )
    imports = ['articles.tasks']



app = Celery()
app.config_from_object(Config)

# app.conf.beat_schedule = {
#     'scheduled': {'task': 'articles.tasks.article_crawler', 'schedule': 60}
# }
