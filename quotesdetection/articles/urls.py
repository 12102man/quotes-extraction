from django.contrib import admin
from django.urls import path

from articles import views

urlpatterns = [
    path('generate_articles', views.generate_articles),
    path('get_spacy_relations', views.get_spacy_relations),
    path('evaluate_quotes_extraction', views.evaluate_quotes_extraction),
]
