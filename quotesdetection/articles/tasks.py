from goose3 import Goose

from articles.models import Article, Source
from quotesdetection.celery import app
import feedparser


@app.task(
    autoretry_for=(Exception,),
    retry_kwargs={'max_retries': 3},
    default_retry_delay=3,
)
def article_crawler():
    """
    Finds new articles and sends to another task

    Sooner or later, Acromantula will be here
    :return:
    """
    pass
    # sources = Source.objects.all()
    # for source in sources:
    #     articles = feedparser.parse(source.feed)
    #     for article in articles.entries:
    #         parsed_article = Goose({'enable_image_fetching': True}).extract(article.link)
    #         obj = Article.objects.create(source=source, text=parsed_article.cleaned_text, url=article.link,
    #                                      title=article.title, description=parsed_article.meta_description)
    #         if not Article.objects.filter(title=article.title).exists():
    #             obj.save()
