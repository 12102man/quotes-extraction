import os
from os import listdir

from django.shortcuts import render

# Create your views here.
from rest_framework import status, decorators
from rest_framework.decorators import api_view
from rest_framework.response import Response
from Levenshtein import distance
from articles.models import Article

from pipeline.Texter import Texter

from pipeline.AnnotationParser import contains_relation, parse_ann, get_relations


def in_list(el, li):
    for e in li:
        if distance(e, el) <= 2:
            return True
    return False


@api_view(('GET',))
@decorators.permission_classes([])
def save_ner(request):
    doc_id = request.GET.get('doc', None)

    if not doc_id:
        return Response({}, status=status.HTTP_404_NOT_FOUND)
    a = Article.objects.get(id=doc_id)
    te = Texter(a.text)
    te.save_ners(doc_id)
    return Response({}, status=status.HTTP_200_OK)


@api_view(('GET',))
@decorators.permission_classes([])
def get_spacy_quotes(request):
    doc_id = request.GET.get('doc', None)

    if not doc_id:
        return Response({}, status=status.HTTP_404_NOT_FOUND)
    a = Article.objects.get(id=doc_id)
    te = Texter(a.text)
    return Response(
        {'q': [x.text for x in te.doc._.quotes], 'sentences': [sent.string.strip() for sent in te.doc.sents]},
        status=status.HTTP_200_OK)


@api_view(('GET',))
@decorators.permission_classes([])
def get_spacy_relations(request):
    doc_id = request.GET.get('doc', None)

    if not doc_id:
        return Response({}, status=status.HTTP_404_NOT_FOUND)
    a = Article.objects.get(id=doc_id)
    te = Texter(a.text)
    return Response(
        {'core': te.get_spacy_relations()},
        status=status.HTTP_200_OK)


@api_view(('GET',))
@decorators.permission_classes([])
def generate_articles(request):
    count = int(request.GET.get('count'))
    for article in Article.objects.all():
        if count == 0:
            break
        if not os.path.isfile(f'./dataset/{article.id}.ann'):
            text = Texter(article.text)
            if len(text.doc._.quotes) > 0:
                with open(f'./dataset/{article.id}.txt', 'w') as f:
                    f.write(article.text)
                with open(f'./dataset/{article.id}.ann', 'w') as f:
                    f.write('')
                text.save_persons(article.id)
                text.save_quotes(article.id)
                count -= 1
    return Response({}, status=status.HTTP_200_OK)


@api_view(('GET',))
@decorators.permission_classes([])
def calculate_accuracy(request):
    files_list = [f for f in listdir('./dataset/') if f.endswith('.ann')]

    average_recall = 0
    average_precision = 0
    average_f1 = 0

    buff = {}

    for file in files_list:
        tp = 0
        fp = 0
        fn = 0
        doc_id = file.split('.')[0]
        with open('./dataset/' + file, 'r') as f:
            standard = []
            for line in f:
                vals = line.split('\t')
                if vals[1].startswith('Quote'):
                    standard.append(" ".join(vals[2:]).replace('\n', ''))
            doc = Article.objects.get(id=doc_id)

            a = Texter(doc.text)
            a.split_to_sentences()
            a.predict_quotes()
            result = a.quotes()

            for q in result:
                if in_list(q, standard):
                    tp += 1
                else:
                    fp += 1

            for s in standard:
                if not in_list(s, result):
                    fn += 1

        recall = 0
        if (tp + fn) > 0:
            recall = tp / (tp + fn)

        precision = 0
        f1 = 0
        if (tp + fp) != 0:
            precision = tp / (tp + fp)

        if precision + recall != 0:
            f1 = 2 * (precision * recall) / (precision + recall)

        buff[doc_id] = {'precision': precision, 'recall': recall,
                        'lengths': {'standard': len(standard), 'result': len(result)},
                        'standard': standard, 'result': result, 'f1': f1}
        average_recall += recall
        average_precision += precision
        average_f1 += f1

    average_precision /= len(buff)
    average_recall /= len(buff)
    average_f1 /= len(buff)

    buff['metrics'] = {'average_precision': average_precision, "average_recall": average_recall,
                       "average_f1": average_f1}

    return Response(buff, status=status.HTTP_200_OK)


@api_view(('GET',))
@decorators.permission_classes([])
def evaluate_quotes_extraction(request):
    files_list = [f for f in listdir('./dataset/') if f.endswith('.ann')]

    average_recall = 0
    average_precision = 0
    average_f1 = 0

    buff = {}

    for file in files_list:
        tp = 0
        fp = 0
        fn = 0
        doc_id = file.split('.')[0]

        p, q, r = parse_ann(file)
        standard = get_relations(p, q, r).values()
        doc = Article.objects.get(id=doc_id)

        a = Texter(doc.text)
        result = list(map(lambda y: y['relations'],
                          filter(lambda x: 'relations' in x and len(x['relations']) > 0, a.get_spacy_relations())))
        flat = []
        for r in result:
            flat.extend(r)
        result = flat

        for r in result:
            if contains_relation(r, standard):
                tp += 1
            else:
                fp += 1

        for s in standard:
            if not contains_relation(s, result):
                fn += 1

        precision = tp / (tp + fp) if tp + fp > 0 else 1
        recall = tp / (tp + fn) if tp + fn > 0 else 1
        f1 = 2 * (precision * recall) / (precision + recall) if (precision + recall) > 0 else 1

        average_precision += precision
        average_recall += recall
        average_f1 += f1

        buff[doc_id] = {'precision': precision, 'recall': recall,
                        'lengths': {'standard': len(standard), 'result': len(result)},
                        'standard': standard, 'result': result, 'f1': f1}

    average_precision /= len(buff)
    average_recall /= len(buff)
    average_f1 /= len(buff)

    buff['metrics'] = {'average_precision': average_precision, "average_recall": average_recall,
                       "average_f1": average_f1}

    return Response(buff, status=status.HTTP_200_OK)
