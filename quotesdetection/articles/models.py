import hashlib

import requests
from django.db import models
from datetime import datetime


class Article(models.Model):
    title = models.CharField(max_length=1024, blank=True)
    url = models.CharField(max_length=256, blank=True)
    source = models.ForeignKey('Source', on_delete=models.DO_NOTHING)
    indexed = models.DateTimeField(default=datetime.now)
    description = models.TextField(default='')
    text = models.TextField(default='')

    @property
    def language(self):
        return self.source.language


    def __str__(self):
        return '{0} by {1}'.format(self.title, self.source.name)


class Source(models.Model):
    EN = 0
    RU = 1
    LANGUAGES = ((EN, 'English'), (RU, 'Russian'))

    name = models.CharField(default="", max_length=1024)
    feed = models.CharField(default="", max_length=1024)
    language = models.IntegerField(choices=LANGUAGES)

    def __str__(self):
        return '{0}'.format(self.name)
