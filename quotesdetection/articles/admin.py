from django.contrib import admin

# Register your models here.
from articles.models import Source, Article
from django.utils.safestring import mark_safe

from pipeline.Texter import Texter

admin.site.register(Source)


class ArticleAdmin(admin.ModelAdmin):
    list_filter = ['source__language']
    readonly_fields = ['highlighted_text']

    def highlighted_text(self, obj):
        a = Texter(obj.text)
        a.split_to_sentences()
        a.predict_quotes()
        # a.extract_ners()
        a.highlight_quotes()
        # a.highlight_ners()
        return mark_safe(a.out_text)


admin.site.register(Article, ArticleAdmin)
