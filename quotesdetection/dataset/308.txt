ABOVE: Christian commentator Todd Starnes Joins CBN News to discuss the discrimination against Franklin Graham

After facing discrimination in the UK because of his biblical beliefs, a spokesman for Franklin Graham tells CBN News they have decided to fight in court for their religious rights there.

"Since the original venues have broken our legal contract with them, we are pursuing appropriate actions based on grounds of religious discrimination and freedom of speech. The Gospel always faces opposition, so we will prayerfully and boldly continue to press forward so that the Good News of God's love and forgiveness will be proclaimed in all of the cities we have planned to visit," the Billy Graham Evangelistic Associaton (BGEA) spokesman told CBN News.

The statement comes after another city in the United Kingdom has rejected an appearance by Graham to preach the gospel because of his views on homosexuality. However, this has not deterred the evangelist who's still looking forward to his eight-city tour of the country.

Wales Online reported that Graham, the eldest son of American preacher Billy Graham, has spoken widely of his view that homosexuality is a "sin" and was booked to speak at the International Convention Centre Wales, Newport, in June, but the event has been canceled.

 The Wales event was the fourth of the evangelist's planned appearances to be canceled. As CBN News reported, the UK cities of Liverpool, Glasgow, and Sheffield earlier canceled their contracts with Graham.

While one venue in Liverpool has denied Graham access, the BGEA tells CBN News a Liverpool outreach will still take place at a different venue that's still to be determined.

Over the weekend, Graham, 67, posted on social media that he's looking forward to "preaching the Gospel across the UK in late May & June."

"I'm looking forward to preaching the Gospel across the UK in late May & June. Hundreds of churches are praying & planning to be a part of these evangelistic outreaches," he wrote on Twitter. "People everywhere are searching for something to fill the void in their lives. Jesus Christ is the answer."

Graham has responded to what he considers the continued effort to bar him from preaching the gospel of Jesus Christ in the UK.

"For some time I have been planning to come and preach the Gospel in eight cities across the UK in 2020," he told Newsweek magazine. "Hundreds of churches are working with the Graham Tour UK and praying for these events."

"Some people have said I am going to bring hateful speech to the UK, but this couldn't be further from the truth," he asserted. "I'm coming to the UK to speak about God's love through His Son Jesus Christ. Jesus didn't come to condemn the world – He came to save it. The Gospel is timeless, and Bible-believing Christians in the UK have proclaimed it for centuries."

Conservative commentator Todd Starnes took to Twitter, writing: "If the LGBT activists are permitted to silence @Franklin_Graham – they will eventually silence all of us. #FightBack

On his website, Starnes also noted the UK's LGBT community is enraged over Graham's appearances in their country.

"They say Graham should not be allowed to speak in public venues – citing his belief that homosexuality is a sin and that marriage is between one man and one woman," Starnes wrote.

"They said his message that Jesus Christ can changes lives might incite hate and put the gay community in danger. So they launched a campaign to bully and badger government leaders into silencing the American evangelist," he continued.

"The rub, I think, comes in whether God defines homosexuality as sin. The answer is yes," Graham explained in a Facebook post. "But God goes even further than that, to say that we are all sinners – myself included. The Bible says that every human being is guilty of sin and in need of forgiveness and cleansing. The penalty of sin is spiritual death – separation from God for eternity," Starnes noted.

Starnes also had a warning for American Christians mentioning that LGBT activists have already conquered the nation's public schools and gained a foothold in some conservative websites and cable news channels.

"If we do nothing they will be successful in silencing Christians in the public marketplace. And once that happens – you'd better believe they will come for our church houses," he wrote.

Graham is not the only preacher to be banned in the UK. The Times reports Larry Stockstill, a Louisiana-based evangelist, also had his appearance in Edinburgh, Scotland canceled after complaints were received over his biblical views on homosexuality.

Despite the reports in the British media, the BGEA spokesman told CBN News they plan to go ahead with the tour.

"The planned Graham Tour is going ahead and is certainly not canceled in any of the cities as some press reports have indicated. The momentum for the Tour is growing throughout the UK by the day. We are continuing to finalize sites for the Tour to determine where the events will take place. In the meantime, all of the preparation events and training programs are continuing as planned," the spokesman said.