The boss of Facebook says he accepts tech giants may have to pay more tax in Europe in future and recognises people's "frustration" over the issue.

Mark Zuckerberg also said he backed plans by think tank the Organisation for Economic Co-operation and Development to find a global solution.

Facebook and others have been accused of not paying their fair share of tax in countries where they operate.

But some say the OECD is moving too slowly towards its goal of a 2020 deal.

In the UK, Facebook paid just £28.5m in corporation tax in 2018 despite generating a record £1.65bn in British sales.

At the time tax campaigner and MP Margaret Hodge said such a low bill was "outrageous", but Facebook said it pays what it owes.

In a conference in Munich this Saturday, Mr Zuckerberg will say: "I understand that there's frustration about how tech companies are taxed in Europe.

"We also want tax reform and I'm glad the OECD is looking at this. We want the OECD process to succeed so that we have a stable and reliable system going forward.

"And we accept that may mean we have to pay more tax and pay it in different places under a new framework."

The UK has said it plans to introduce its own digital services tax in April, despite US objections, in a move that could generate up to £500m a year.

However, it is unclear how the resignation of Sajid Javid as chancellor - a major supporter of the tax - will affect the move.

France has agreed to postpone its own digital sales tax, but only until the end of the year, pending a global agreement. Washington had threatened to impose tariffs on French champagne and cheese in retaliation.

Many governments are concerned that US technology giants are avoiding taxes in the European Union. They argue taxes should be based on where the digital activity - browsing the page - takes place, not where firms have their headquarters.

In response the UK, along with several other European countries, have proposed new tax rules.

Britain, for example, would tax the revenues of search engines, social media platforms and online marketplaces at 2%. France's goal has been 3%.

But trade officials in Washington say US firms are being unfairly targeted.

In January US Treasury Secretary Steve Mnuchin threatened new tariffs on UK carmakers, arguing the digital tax would be "discriminatory in nature".