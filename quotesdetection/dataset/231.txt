Antarctica has exceeded 20C for the first time, after researchers logged a temperature of 20.75C on an island off the coast of the continent.

Brazilian scientist Carlos Schaefer told AFP they had "never seen a temperature this high in Antarctica".

But he warned the temperature, logged on 9 February, was just one reading and not part of a long-term data set.

The continent also hit a record last week, with a temperature reading of 18.3C on the Antarctic Peninsula.

This latest reading was taken at a monitoring station on Seymour Island, part of a chain of islands off the same peninsula, at the northernmost point of the continent.

Although the temperature is a record high, Mr Schaefer emphasized that the reading was not part of a wider study and so, in itself, could not be used to predict a trend.

"We can't use this to anticipate climatic changes in the future. It's a data point," he said. "It's simply a signal that something different is happening in that area."

According to the UN's World Meteorological Organisation (WMO), temperatures on the Antarctic continent have risen by almost 3C over the past 50 years, and that about 87% of the glaciers along its west coast have "retreated" in that time.

Over the past 12 years the glaciers have shown an "accelerated retreat" due to global warming, it adds.

Last month was also Antarctica's warmest January on record.

Scientists have warned that global warming is causing so much melting at the South Pole, it will eventually disintegrate - causing the global sea level to rise by at least three metres (10ft) over the coming centuries.

The previous record for the entire Antarctic region - which includes the continent, islands and ocean that are in the Antarctic climatic zone - was 19.8C, logged in January 1982.

Last July, the Arctic region hit its own record temperature of 21C, logged by a base at the northern tip of Ellesmere Island in the Canadian Arctic.