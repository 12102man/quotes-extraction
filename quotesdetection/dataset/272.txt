China is taking some tough measures to control the coronavirus as the death toll climbs past 1,100 with more than 100 dying in just one day there.

But China's national health commission said just over 2,000 new cases were reported over the previous 24 hours, which would be a decline in the rate of new cases for the second day.

The total number of reported global cases is now around 45,000, although many experts say a large number of others who have been infected have not been counted.

Authorities in China are going to extremes to stop the spread of the contagion. One video showed health officials forcibly removing a Chinese couple from their home, reportedly for refusing to self-quarantine.

And a masked President Xi Jinping visited a health center in Beijing as citizens continue to question the communist government's handling of the spreading disease.

President Trump says he spoke with the Chinese leader over the weekend. "He feels very confident," Trump said. "During the month of April, the heat generally speaking kills this kind of virus so that would be a good thing but we are in great shape in our country."

Thirteen people in the US have been infected with the coronavirus. The latest case has been confirmed in California.

Meanwhile, the World Health Organization convened a meeting of 400 scientists on Tuesday to try to speed the development of tests, treatments and vaccines against the new virus now named COVID-19.

There are no proven treatments or vaccines for the new and still-mysterious virus.

"It's hard to believe that just two months ago, this virus — which has come to captivate the attention of media, financial markets and political leaders — was completely unknown to us," WHO director-general Tedros Adhanom Ghebreyesus said at the start of the meeting.

Experts say it could be months or even years before any approved treatments or vaccines are developed, by which time the outbreak might be over. But they say they will at least have more weapons at their disposal if the virus strikes again.