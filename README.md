## Quotes detection in text and their extraction

This project is being developed in Innopolis University by Mikhail Tkachenko (BS17-DS-01) under supervision of Professor Vladimir Ivanov.


#### Goal
Goal of this project is to create an algorithm which detects quotes in a given text and detects relation between them and persons mentioned in text.

#### Quotes
To detect a quote in a text, it should be defined by some restrictions:

- There can not be a quote inside a quote
- There may be a mention of an object in quotation marks inside the quote. Example:
> "I don't think that restaurant "Ugly Coyote" will be closed" Ivan said.
-  Quote always has an author. Example: 
> "Most of these cases relate to a period going back over 
days and weeks and are retrospectively reported as 
cases since sometimes back to the beginning of the 
outbreak itself," he said.
- Quote is longer than 3 words. 
This requirement is needed to separate quotes from entities.

- Quote may be in any place of sentence.
- Quote may contain multiple sentences inside.

#### Pipeline
1. Split text into sentences. This is important to satisfy Rule 5
2. Replace all quotation marks in text to " : ' => ", and others. 
3. Perform quotes detection
    1. Record position of quotes in text.
    2. Find all quotation marks in text. Their number should be even.
    
    ... to be continued 


#### Anomalies
1. End mark (dot, excl. mark, etc) position:
    "blablabla." or 
    "blablabla". or 
    "blablabla.".
2. Quote in quote
    
 An hour after I paid my £3 I got quite a few emails from people... and I thought 'wow, these are so much better than people on other sites'".
 
 
 #### Quotes detection
 Quotes are detected easily: replace all ' with " and detect quotes between n-1th and nth quote. For now it gives 88% precision and 82% recall.
 
 #### Persons detection
 Persons are detected as 'Person' NERs from spacy library. The biggest advantage of spacy is that it keeps indices of tokens, so that results can be combined with BRAT annotation tool.
 
 #### Relations
 Relations are detected easily for now: if there's one quote in sentence and one person, they are assumed to be connected. 
 TODO: try LSTM, but for this another annotation is required.
 
 Precision: 64%
 Recall: 74%
 F1: 72%
 
 As we see from statistics, there are many false positives. This is easily explained: current approach does not involve semantics of sentence. 
 
 Example: It was like the patients were "stuck in Groundhog Day" - the 1993 comedy where Bill Murray's character relives the same 2 February day over and over - but were instead stuck obsessively reliving their painful betrayals in their minds.
 
 Bill Murray is not "stuck in Groundhog Day". However, for detector it is a quote.

 #### Further improvements
 For detecting such "unquotes" something semantic is required. What wbout using LSTM with POS tags, where quote is replaced into a single tag? Fot example:
 
 "I do not like Bill" - Maria said."
 
 ["Q", "NNP", "VERB"]
 
 We see semantic here and connection through a verb. However, this is another story...
  